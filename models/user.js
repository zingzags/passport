var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
const saltRounts = 10;

//The user Schema
var userSchema = mongoose.Schema({
  username: {
    type: String,
    index:true
  },
  password: {
    type: String
  },
  email: {
    type: String
  },
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  gender: {
    type: String
  }
});

var user = module.exports = mongoose.model('user', userSchema);

module.exports.createUser = function(newUser, callback) {
  bcrypt.genSalt(saltRounts, function(err, salt) {
    bcrypt.hash(newUser.password, salt, function(err, hash) {
      newUser.password = hash;
      newUser.save(callback);
    });
  });
}

module.exports.getUserByUsername = function(username, callback) {
  var query = {username: username};
  user.findOne(query, callback);
}

module.exports.comparePassword = function(candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
    if (err) throw err;
    callback(null, isMatch);
  });
}

module.exports.findUserById = function(id, callback) {
  user.findById(id, callback);
}
