var express = require('express');//These are all coming from the dependencies inside of the package.json file
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var mongo = require('mongodb');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/passport');
var db = mongoose.connection;﻿

//Routes - As the name states, it routes the connection elsewhere
var routes = require('./routes/index');
var users = require('./routes/users');
var auth = require('./routes/auth');

//Init
/*
Express.js is a node.js framework which helps organizing your web apps

Express is a MVC architecture

MVC architecture - (Model-View-Controller) its an architecture patten used for
developing user interfaces that divides an application into three interconnected parts.

it seperates the three different components in the user interface.

View: What the user see's in their interface

Controller: What the user is allowed to control/manipulate inside the interface
(View and Controller live on the same level)

Model: It directly manages the data, logic, and it rules the application

Which then loops around, and updates the Views

So for example lets say I see a button that says "Show More", and I click on it,
because its bound to a controller, it gets sent to the model, and the model will
update the view showing what was 'hidden behind' the "Show More" button.

*/
var app = express();

//View engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({defaultLayout:'layout'}));
app.set('view engine', 'handlebars');

//BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

/*Set Static Folder - The folder which will hold all the public data which
will be displayed on the page for the end user. (ex images, css, etc)*/
app.use(express.static(path.join(__dirname, 'public')));

//Express session
app.use(session({
  secret: 'secret',
  saveUninitialized: true,
  resave: true
}));

//Passport saveUninitialized
app.use(passport.initialize());
app.use(passport.session());

//Express Validator Middleware - Middleware which helps validate user input
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
    var namespace = param.split('.');
    var root = namespace.shift()
    var formParam = root;

    while (namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

/*Connect flash -Which is a messaging System that utilizes the session for
temp storage, mainly used for error/success message*/
app.use(flash());

/*Global Vars - Variables for the flash message that will display the status
of the user authentication system
*/
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/auth', auth);

//Setting a port to the program
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function() {
  console.log('Server started on port ' + app.get('port'));
});
