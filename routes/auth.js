var express = require('express');
var router = express.Router();
var User = require('../models/user');
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var openIdStrategy = require('passport-openid').Strategy;

//Get register
router.get('/register', function(req, res) {
  res.render('register');
});

//Get Login
router.get('/login', function(req, res) {
  res.render('login');
});

router.get('/logout', function(req, res) {
  req.logout();
  req.flash('success_msg', 'You have logged out.');
  res.redirect('/');
});

//Login user
router.post('/login',
  passport.authenticate('local', {failureRedirect:"/auth/login", failureFlash: true}), function(req, res) {
    res.redirect('/users/' + req.user.username);
});

//Register User
router.post('/register', function(req, res) {
  var firstname = req.body.firstname;
  var lastname = req.body.lastname;
  var email = req.body.email;
  var username = req.body.username;
  var password = req.body.password;
  var password2 = req.body.password2;
  var gender = req.body.gender;

  //Validation
  req.checkBody('firstname', 'The First name is required.').notEmpty();
  req.checkBody('lastname', 'The Last name is required.').notEmpty();
  req.checkBody('email', 'The Email is required.').notEmpty();
  req.checkBody('email', 'The Email is not valid.').isEmail();
  req.checkBody('password', 'The Password is required.').notEmpty();
  req.checkBody('password2', 'You need to confirm the Password.').notEmpty();
  req.checkBody('password2', 'The Passwords do not match.').equals(password);

  var errors = req.validationErrors();
  if (errors) {
    res.render('register', {
      errors:errors
    });
  } else {
    var newUser = new User({
      firstname: firstname,
      lastname: lastname,
      email: email,
      username: username,
      password: password,
      gender: gender
    });

    User.createUser(newUser, function(err, user) {
      if (err)
        throw err;
    });
    req.flash('success_msg', 'You have been registered, please check you email for a confirmation link.');

    req.login(newUser, function(err) {
      if (err) {
        throw err;
      }
      res.redirect('/users/' + username);
    });
  }

});

passport.use(new localStrategy(
  function(username, password, done) {
    User.getUserByUsername(username, function(err, user) {
      if (err)
        throw err;
      if (!user) {
        return done(null, false, {message: 'User does not exist.'});
      }
      User.comparePassword(password, user.password, function(err, isMatch) {
        if (err)
          return done(err);
        if (isMatch) {
          return done(null, user);
        } else {
          return done(null, false, {message: 'Invalid password.'});
        }
      });
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findUserById(id, function(err, user) {
    done(err, user);
  });
});

module.exports = router;
