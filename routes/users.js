var express = require('express');
var router = express.Router();

var hasAuthentication = function(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('error_msg', 'You are not logged in.');
    res.redirect('/');
  }
}

//Get Users page
router.get('/:username', hasAuthentication, function(req, res) {
  res.render('username');
});

module.exports = router;
